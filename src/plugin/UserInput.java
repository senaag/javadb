/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

/**
 *
 * @author sena
 */
public class UserInput {
    //buat variable untuk menampung data input dari frame input
    private static String username;
    private static String namalengkap;
    private static String pasword1;
    private static String pasword2;
    private static Object keterangan;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        UserInput.username = username;
    }

    public static String getNamalengkap() {
        return namalengkap;
    }

    public static void setNamalengkap(String namalengkap) {
        UserInput.namalengkap = namalengkap;
    }

    public static String getPasword1() {
        return pasword1;
    }

    public static void setPasword1(String pasword1) {
        UserInput.pasword1 = pasword1;
    }

    public static String getPasword2() {
        return pasword2;
    }

    public static void setPasword2(String pasword2) {
        UserInput.pasword2 = pasword2;
    }

    public static Object getKeterangan() {
        return keterangan;
    }

    public static void setKeterangan(Object keterangan) {
        UserInput.keterangan = keterangan;
    }
    
    public static void hapus(){
        username = "";
        namalengkap = "";
        pasword1 = "";
        pasword2 = "";
        keterangan = "";
    }
}

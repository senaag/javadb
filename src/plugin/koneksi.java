/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author sena
 */
public class koneksi {
    //buat variabel koneksi
    private Connection koneksi;
    
    //buat metode untuk merubah/menjalankan koneksi
    public Connection connect(){
        //cek apakah driver/library database/mysql tersedia
        try{
            //cek driver/library
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println("Berhasil koneksi");
        }catch(ClassNotFoundException ex){
            //jalankan perintah jika kode di atas error
            System.out.println("Gagal Koneksi "+ex);
        }
        
        //alamat database
        String url = "jdbc:mysql://localhost:3306/tutordb";
        //cek apakah database aktif
        try{
            //cek koneksi database
            koneksi = DriverManager.getConnection(url, "root", "");
            System.out.println("Berhasil koneksi database");
        }catch(SQLException ex){
            System.out.println("Gagal Koneksi Database " + ex);
        }
        return koneksi;
    }
}
